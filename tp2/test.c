#include <stdio.h>
#include "entier.h"
#include "rat.h"
#include "munit.h"

#define UNUSED(x) (void)(x)

MunitResult test_somme(const MunitParameter params[],
                       void *user_data_or_fixture) {
  UNUSED(params);
  UNUSED(user_data_or_fixture);

  int source[502];
  for (int i = 1; i < 501; ++i)
    source[i] = i;
  source[0] = -10000;
  source[501] = -20000;

  int sum = 500 * (500 + 1) / 2; // somme des i premiers entiers de 1 à 500
  munit_assert_int(sum, ==, somme(source + 1, 500));
  for (int i = 1; i < 501; ++i) {
    munit_assert_int(source[i], ==, i);
  }
  munit_assert_int(source[0], ==, -10000);
  munit_assert_int(source[501], ==, -20000);

  return MUNIT_OK;
}

MunitResult test_copie_dans(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);

  int source[502];
  for (int i = 1; i < 501; ++i)
    source[i] = munit_rand_int_range(200, 800);
  source[0] = -10000;
  source[501] = -20000;

  int destination[502];
  for (int i = 1; i < 501; ++i)
    source[i] = munit_rand_int_range(200, 800);
  destination[0] = -2000;
  destination[501] = -4000;

  copie_dans(destination + 1, source + 1, 500);

  munit_assert_int(destination[0], ==, -2000);
  munit_assert_int(destination[501], ==, -4000);
  for (int i = 1; i < 501; ++i)
    munit_assert_int(source[i], ==, destination[i]);

  return MUNIT_OK;
}

MunitResult test_ajoute_apres(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);

  int source[] = {-2000, 5, 4, 5, 10, 20, -3000};
  int dest[] = {-5000, 90,    100,   20,    -1500, -2500,
                -3500, -4500, -5500, -6500, -7500, -8500};
  int expected[] = {-5000, 90, 100, 20, 5, 4, 5, 10, 20, -6500, -7500, -8500};
  ajoute_apres(dest + 1, 3, source + 1, 5);
  const int s = sizeof(dest) / (sizeof(dest[0]));
  for (int i = 0; i < s; ++i) {
    munit_assert_int(dest[i], ==, expected[i]);
  }
  return MUNIT_OK;
}

MunitTest tests_entiers[] = {
    {
        "/somme",               /* name */
        test_somme,             /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },
    {
        "/copie-dans",          /* name */
        test_copie_dans,        /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },

    {
        "/ajoute_apres",        /* name */
        test_ajoute_apres,      /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },

    /* Mark the end of the array with an entry where the test
     * function is NULL */
    {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};

static const MunitSuite suite_entiers = {
    "/liste-entiers",       /* name */
    tests_entiers,          /* tests */
    NULL,                   /* suites */
    300,                    /* iterations */
    MUNIT_SUITE_OPTION_NONE /* options */
};


/******************* rat */

MunitResult test_rat_produit(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);

  struct rat a = {.num = 2, .den = 3};
  struct rat b = {.num = 4, .den = 5};
  struct rat r = rat_produit(a, b);
  munit_assert_int(r.num, ==, 8);
  munit_assert_int(r.den, ==, 15);
  return MUNIT_OK;
}

MunitResult test_rat_somme(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);

  struct rat a = {.num = 2, .den = 3};
  struct rat b = {.num = 4, .den = 5};
  struct rat r = rat_somme(a, b);
  munit_assert_int(r.num, ==, 22);
  munit_assert_int(r.den, ==, 15);
  return MUNIT_OK;
}

MunitResult test_rat_plus_petit(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);
  struct rat list[] = {
    {.num = 1, .den = 2},
    {.num = 1, .den = 4},
    {.num = 2, .den = 5},
    {.num = 1, .den = 8},
    {.num = 1, .den = 0},
  };
  struct rat r = rat_plus_petit(list);
  munit_assert_int(r.num, ==, 1);
  munit_assert_int(r.den, ==, 8);

  struct rat list2[] = {
    {.num = 1, .den = 8},
    {.num = 1, .den = 2},
    {.num = 1, .den = 4},
    {.num = 2, .den = 5},
    {.num = 1, .den = 0},
  };
  r = rat_plus_petit(list2);
  munit_assert_int(r.num, ==, 1);
  munit_assert_int(r.den, ==, 8);


  struct rat list3[] = {
    {.num = 1, .den = 8},
    {.num = 1, .den = 0},
  };
  r = rat_plus_petit(list3);
  munit_assert_int(r.num, ==, 1);
  munit_assert_int(r.den, ==, 8);

  struct rat list4[] = {
    {.num = 12, .den = 0},
    {.num = 12, .den = 3},
    {.num = 12, .den = 4},
  };
  r = rat_plus_petit(list4);
  munit_assert_int(r.num, ==, 1);
  munit_assert_int(r.den, ==, 0);
  
  return MUNIT_OK;
}

#ifdef RAT_SIMPL
MunitResult test_rat_simplifie(const MunitParameter params[], void *ud) {
  UNUSED(params);
  UNUSED(ud);
  
  struct rat r = {.num = 1, .den = 2 };
  struct rat r2 = {.num = 2, .den = 3 };
  struct rat r3 = {.num = 4, .den = 8 };
  struct rat r4 = {.num = 25, .den = 75 };
  struct rat r5 = {.num = 26000000, .den = 75000000 };
  struct rat r6 = {.num = -3, .den = 12 };

  struct rat s;
  s = rat_simplifie(r);
  munit_assert_int(s.num, ==, 1);
  munit_assert_int(s.den, ==, 2);

  s = rat_simplifie(r2);
  munit_assert_int(s.num, ==, 2);
  munit_assert_int(s.den, ==, 3);

  s = rat_simplifie(r3);
  munit_assert_int(s.num, ==, 1);
  munit_assert_int(s.den, ==, 2);

  s = rat_simplifie(r4);
  munit_assert_int(s.num, ==, 1);
  munit_assert_int(s.den, ==, 3);

  s = rat_simplifie(r5);
  munit_assert_int(s.num, ==, 26);
  munit_assert_int(s.den, ==, 75);

  s = rat_simplifie(r6);
  munit_assert_int(s.num, ==, -1);
  munit_assert_int(s.den, ==, 4);

  return MUNIT_OK;
}
#endif

MunitTest tests_rat[] = {
    {
        "/produit",               /* name */
        test_rat_produit,             /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },
    {
        "/somme",               /* name */
        test_rat_somme,             /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },
    {
        "/plus_petit",               /* name */
        test_rat_plus_petit,             /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },
#ifdef RAT_SIMPL
    {
        "/simplifie",               /* name */
        test_rat_simplifie,             /* test */
        NULL,                   /* setup */
        NULL,                   /* tear_down */
        MUNIT_TEST_OPTION_NONE, /* options */
        NULL                    /* parameters */
    },
#endif
    /* Mark the end of the array with an entry where the test
     * function is NULL */
    {NULL, NULL, NULL, NULL, MUNIT_TEST_OPTION_NONE, NULL}};

static const MunitSuite suite_rat = {
    "/rat",       /* name */
    tests_rat,          /* tests */
    NULL,                   /* suites */
    300,                    /* iterations */
    MUNIT_SUITE_OPTION_NONE /* options */
};

int main(int argc, char **argv) {
#ifndef RAT_SIMPL
  printf("Recompiler avec -DRAT_SIMPL pour activer le test de simplification de fraction\n");
#endif
  int ret = munit_suite_main(&suite_entiers, NULL, argc, argv);
  if (ret != 0)
    return ret;
  return munit_suite_main(&suite_rat, NULL, argc, argv);
}
