#include <ctype.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define BUFFER_SIZE 80

int traiter (int f, int *car, int *mot, int *lig){
	 char buffer[BUFFER_SIZE];

	 int ret = 0;
	 int ismot = 0;
	 *car = *car - *car;
	 *mot = *mot - *mot;
	 *lig = *lig - *lig;

	while((ret = read(f,buffer,BUFFER_SIZE)) > 0){
		*car += ret;
		for (int i = 0; i < ret; ++i){
			//printf("%c\n", buffer[i]);
			if(isspace(buffer[i]) != 0 && ismot == 0){
				ismot = 1;
				*mot = *mot +1;
			}
			if(isspace(buffer[i]) == 0 && ismot == 1){
				ismot = 0;
			}

			if (buffer[i] == '\n'){
				//printf("%d %d %d\n",*car,*mot,*lig);
				*lig = *lig +1;
				if (f == 1)
				{
					*car = *car +1;
					return 0;
				}
			}
		}
		if (ret == -1)
			return 1;
	}
	if(ismot == 0){
		*mot = *mot +1;
	}
	if (ret == -1)
		return 1;
	//printf("%d %d %d\n",*car,*mot,*lig);
	return 0;
}

void erreur(){
	printf("Syntax de la command : ./wc [-l|-w] (file|files|text)\n");
}